const express = require('express')
const app = express()
const Usuario = require('../models/user')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

app.post('login', (req, res) => {
    body = req.body
    Usuario.findOne({
        user: body.user
    }, (err, result)) => {
        if (err) {
            res.status(500).json({
                ok: false,
                err
            })
            if (!result) {
                res.status.json(400).json({
                    ok: true,
                    result
                })
            }
            const match = bcrypt.compareSync(body.password)
            if (match) {
                let token = jwt.sign({
                    usuario: result
                }, process.env.SEED, {
                    expiresIn: process.env.CADUCIDAD
                });
            } else {
                res.status(400).json({
                    ok: false,
                    msg: "contraseña incorrecta"
                })
            }

        }
    }

})