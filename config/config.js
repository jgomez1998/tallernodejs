// 
// PORT
// 


// Environment
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// BD

let urlDB;
if (process.env.NODE_ENV == 'dev') {
    urlDB = 'mongodb://localhost:27017/sga'

} else {
    urlDB = process.env.MONGO_URI;
}

process.env.URLDB = urlDB;
process.env.PORT = process.env.PORT || 3000;
//SEED
process.env.SEED = process.env.SEED || "seed-secreto";
//CADUCIDAD
process.env.CADUCIDAD = process.env.CADUCIDAD || '30d'